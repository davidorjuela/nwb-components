import React, { useState } from 'react';
import {render} from 'react-dom';
import styled, { createGlobalStyle } from 'styled-components';
import CardBurger from '../../src/CardBurger';
import CardDocker from '../../src/CardDocker';
import CardEmpezarProy from '../../src/CardEmpezarProy';
import CardOrder from '../../src/CardOrder';
import CardPeople from '../../src/CardPeople';
import CardPlans from '../../src/CardPlans';
import CardSuscribete from '../../src/CardSuscribete';
import CardTokenLimit from '../../src/CardTokenLimit';
import CardTryPro from '../../src/CardTryPro';
import FooterContacto from '../../src/FooterContacto';
import HistorialImp from '../../src/HistorialImp';
import CardRepo from '../../src/CardRepo';

const GlobalStyle = createGlobalStyle`
html{
  box-sizing: border-box;
  margin: 0px;
  padding: 0px;
  font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  overflow: hidden;
}
body{
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}
*, *:before, *:after {
    box-sizing: inherit;
    margin: 0px;
    padding: 0px;
}
`
const Container = styled.div`
  position: relative;
  width: 100%;
  height: 100vh;
`
const Text = styled.p`
  font-size: .9rem;
  margin: 0px 20px 0px 15px;
  color: #444;
  ${props => props.dark && `font-size: 1.5rem; font-weight: 800;`}
`
const Row = styled.div`
position: relative;
display: flex;
justify-content: space-between;
width: 100%;
padding: 15px;
background-color: #DDA;
`
const Slider = styled.div`
position: relative;
display: flex;
height: 150px;
min-width: 50%;
`
const Button = styled.div`
  font-size: 10rem;
  color: #AAA;
  opacity: 0.5;
  cursor: pointer;
  position: absolute;
  line-height: 60%;
  z-index: 10;
  border-radius: 50%;
  overflow: hidden;
  top: calc((100vh - 165px + 1.5*10rem)/2);
  ${props => props.left && `left: 20px;`}
  ${props => props.right && `right: 20px;`}
`
const Image = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  height: 150px;
  width: 100%;
  background-repeat: no-repeat;
  background-position: 0px 0px;
  background-size: contain;
  ${props => props.src&& `background-image: url(${props.src})`};
`
const Box = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-end;
width: 100%;
height: 100%;
`
const Content = styled.div`
display: block;
width: 100%;
height: 100%;
text-align: center;
background-color: #333;
padding: 15px;
overflow-y: auto;
`
const App = () => {

  const components = [
    <CardBurger/>,
    <CardDocker email={'davidorjuel@gmail.com'}
                iniciales = {'DO'}/>,
    <CardEmpezarProy/>,
    <CardOrder/>,
    <CardPeople/>,
    <CardPlans  title={'You Have Pricing Plans'}
                description={'Click Manage to create your first plan.'}
                src={'https://cdn.pixabay.com/photo/2017/03/08/14/20/flat-2126884_960_720.png'}
                bText={'Got it'}/>,
    <CardSuscribete mejorOpcion = {true}
                    periocidad={12}// periocidad > 0 (meses)
                    precioMes={7.50}
                    precioMesPromo={6.75}
                    moneda ={'EUR'}/>,
    <CardTokenLimit title={"You've reached your 1 token limit"}
                    description={'Deactivate your existing tokens, or get more tokens when you upgrade your subscription plan.'}
                    b1Text={'Back to Tokens'}
                    b2Text={'Upgrade'}/>,
    <CardTryPro/>,
    <FooterContacto/>,
    <HistorialImp/>,
    <CardRepo></CardRepo>
  ];



  const [index, setIndex] = useState(0);

  function changeIndex (value){

    const x = index + value;

    if(x===components.length) setIndex(0);

    else if(x===-1) setIndex(components.length-1);

    else setIndex(x);

  }

  function getKey (e) {
    if (e.keyCode === 37) changeIndex(-1);
    if (e.keyCode === 39) changeIndex(1);
  }

  return(
    <Container onKeyDown={(e)=>getKey(e)} tabIndex={'0'}>
      <GlobalStyle />
      <Box>
        <Row>
          <Box>
            <Text>nwb-component #{index+1} de David Orjuela</Text>
            <Text dark>{components[index].type.name}</Text>
          </Box>
          <Slider>
            <Image src={`../../src/${components[index].type.name}/image.jpeg`} />
            <Image src={`../../src/${components[index].type.name}/image.png`} />
          </Slider>
          <Button left onClick={() => changeIndex(-1)}>&lt;</Button>
          <Button right onClick={() => changeIndex(1)}>&gt;</Button>
        </Row>
        <Content>
          {components[index]}
        </Content>
      </Box>
    </Container>
  ) 
}

render(<App/>, document.querySelector('#demo'))

