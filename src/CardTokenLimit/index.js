import React from 'react';
import styled from 'styled-components';

const Card = styled.div`
background-color: #FFF;
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-start;
width: 600px;
height: fit-content;
margin: 0px auto;
padding: 30px;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Title = styled.p`
  margin: 0px;
  color: #333;
  font-weight: 600;
  font-size: 1.05rem;
  font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
`
const Description = styled.p`
  text-align: left;
  color: #555;
  font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
  font-weight: 100;
  font-size: 0.8rem;
  letter-spacing: 0.8px;
  margin: 15px 0px 35px 0px;
`
const Span = styled.span`
  color: #48A0E0;
  cursor: pointer;
`
const Row = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  width: 100%;
`

const Button1 = styled.button`
  margin-left:15px;
  background-color: white;
  color: #007AFF;
  font-size: 0.8rem;
  font-weight: 600;
  padding: 8px 20px;
  border: 2px solid #007AFF;;
  border-radius: 3px;
`

const Button2 = styled.button`
  margin-left:15px;
  background-color: #007AFF;
  color: white;
  font-size: 0.8rem;
  font-weight: 600;
  padding: 8px 20px;
  border: 2px solid #007AFF;;
  border-radius: 3px;
`

const CardTokenLimit = ({title, description, b1Text, b2Text}) => {
  return(

      <Card>
        <Title>{title}</Title>
        <Description>{description} <Span>Learn more</Span></Description>
        <Row>
          <Button1>{b1Text}</Button1>
          <Button2>{b2Text}</Button2>
        </Row>
      </Card>

  )
}

export default CardTokenLimit
