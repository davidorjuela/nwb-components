import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import data  from './data'

const sizes = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px',
};

const devices = {
  mobileS: `(min-width: ${sizes.mobileS})`,
  mobileM: `(min-width: ${sizes.mobileM})`,
  mobileL: `(min-width: ${sizes.mobileL})`,
  tablet: `(min-width: ${sizes.tablet})`,
  laptop: `(min-width: ${sizes.laptop})`,
  laptopL: `(min-width: ${sizes.laptopL})`,
  desktop: `(min-width: ${sizes.desktop})`,
};

const Container = styled.div`
display: flex;
flex-direction: column;
flex-wrap: wrap;
justify-content: center;
align-items: baseline;
align-content: center;
width: 100%;
overflow-y: auto;
background-color: #DDD;
padding: 30px;
border-radius: 5px;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Row = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
align-content: center;
width: 100%;
height: 100%;
padding: 1rem;
${props => props.bordered && `border-top: 1px solid #CCC`}
`
const Box = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
align-content: center;
width: 70%;
${props => props.padding&& `padding: 15px; width: 100%;`}
${props => props.width && `width: ${props.width}`}
`
const Text = styled.p`
width: 27%;
text-align: left;
font-size: .9rem;
color: #777;
${props => props.bold && `font-weight: bold;`}
${props => props.lwidth && `width: 19%;`}
`
const Title = styled.p`
width: 100%;
font-size: 1.3rem;
text-align: left;
color: #777;
margin-bottom: 10px;
`
const Description = styled.p`
width: 100%;
font-size: .8rem;
text-align: left;
color: #999;
`
const Button = styled.div`
display: flex;
justify-content: center;
align-items: center;
align-content: center;
color: #FFF;
background-color: #3C78EE;
font-size: .8rem;
font-weight: bold;
border-radius: 5px;
padding: .3rem 1rem;
margin-right: 60px;
cursor: pointer;
::before{
  content: "+";
  font-size: 1.4rem;
  margin-right: 10px;
}
`
const Icon = styled.div`
display: inline-block;
position: relative;
top: .2rem;
width: 1.1rem;
height: 1.1rem;
border-radius: 50%;
border-width: 2px;
border-style: solid;
opacity: 0.3;
margin-right: 10px;
${props => props.state=="Completado" && `border-color: green; background-color: #AFA;`}
${props => props.state=="Con errores" && `border-color: #FA0; background-color: #FF5;`}
${props => props.state=="No importado" && `border-color: red; background-color: #FAA;`}
`
const HistorialImp = () => {

  return(
    <Container>
      <Row>
        <Box>
          <Title>Historial e importaciones manuales</Title>
          <Description>Revisa los resultados por cada actualización</Description>
        </Box>
        <Button>Importación masiva</Button>
      </Row>
      <Box padding>
        <Row>
          <Text bold>Fecha solicitud</Text>
          <Text bold>Estado</Text>
          <Text bold>Tipo de información</Text>
          <Text lwidth bold>Solicitado por</Text>
        </Row>
        {data.length>0 && data.map(item => 
          <Row bordered>
            <Text>{item.date}</Text>
            <Text><Icon state={item.state}/>{item.state}</Text>
            <Text>{item.type}</Text>
            <Text lwidth>{item.user}</Text>
          </Row>   
      )}
      </Box>
    </Container>
  )
}

export default HistorialImp
