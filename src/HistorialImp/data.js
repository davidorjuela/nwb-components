const data = [
    {
        "date": "18 Mayo, 2020",
        "state": "Completado",
        "type": "Registros pendientes",
        "user": "Maria Rodríguez"
    },
    {
        "date": "18 Mayo, 2020",
        "state": "No importado",
        "type": "Registros pendientes",
        "user": "Maria Rodríguez"
    },
    {
        "date": "24 Diciembre, 2019",
        "state": "Completado",
        "type": "Registros pendientes",
        "user": "Maria Rodríguez"
    },
    {
        "date": "24 Diciembre, 2019",
        "state": "Con errores",
        "type": "Registros pendientes",
        "user": "Maria Rodríguez"
    },
    {
        "date": "18 Septiembre, 2020",
        "state": "Completado",
        "type": "Registros pendientes",
        "user": "Maria Rodríguez"
    },
    {
        "date": "17 Septiembre, 2020",
        "state": "No importado",
        "type": "Registros pendientes",
        "user": "Maria Rodríguez"
    },
    {
        "date": "03 Junio, 2019",
        "state": "Completado",
        "type": "Registros pendientes",
        "user": "Maria Rodríguez"
    },
    {
        "date": "03 Junio, 2019",
        "state": "Con errores",
        "type": "Registros pendientes",
        "user": "Maria Rodríguez"
    }
];


export default data