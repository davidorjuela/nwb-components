import React from 'react';
import styled from 'styled-components';

const Card = styled.div`
background-color: #FFF;
position: relative;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
width: 350px;
height: fit-content;
padding: 30px;
margin: 0px auto;
border-radius: 7px;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Title = styled.p`
  margin: 25px 0px 0px 0px;
  color: #333;
  font-weight: 500;
  font-size: 1.2rem;
  font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
`
const Description = styled.p`
  margin: 15px 0px 0px 0px;
  color: #777;
  font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
  font-weight: 100;
  font-size: 0.9rem;
  letter-spacing: 0.8px;
`
const Image = styled.img`
  margin-top: 30px;
  width: 50%;
  height: auto;
  margin: 0 auto;
  margin-top: 30px;
`
const Icon = styled.img`
  width: 15px;
  height: 15px;
  position:absolute;
  top: 30px;
  right: 30px;
  cursor: pointer;
  opacity: 0.75;
  margin: 0px;
`
const Button = styled.button`
  margin: 30px 0px 20px 0px;
  background-color: #449DEF;
  color: white;
  font-size: 0.8rem;
  border-radius: 25px;
  font-weight: 100;
  padding: 10px 25px;
  border: none;
`

const CardPlans = ({title, description,src, bText}) => {
  return(
    <Card>
      <Icon src={'https://www.nicepng.com/png/detail/128-1281858_deletex-comments-delete-icon-svg.png'} />
      <Image src={src}/>
      <Title>{title}</Title>
      <Description>{description}</Description>
      <Button>{bText}</Button>
    </Card>
  )
}

export default CardPlans
