import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

const sizes = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px',
};

const devices = {
  mobileS: `(min-width: ${sizes.mobileS})`,
  mobileM: `(min-width: ${sizes.mobileM})`,
  mobileL: `(min-width: ${sizes.mobileL})`,
  tablet: `(min-width: ${sizes.tablet})`,
  laptop: `(min-width: ${sizes.laptop})`,
  laptopL: `(min-width: ${sizes.laptopL})`,
  desktop: `(min-width: ${sizes.desktop})`,
};

const Container = styled.div`
display: flex;
flex-wrap: wrap;
justify-content: center;
align-items: baseline;
align-content: center;
width: 100%;
overflow-y: auto;
background-color: #DDD;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Card = styled.div`
background-color: #FFF;
color: #000;
position: relative;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
align-content: center;
width: 100%;
height: fit-content;
padding: 15px 20px;
margin: 10px;
@media ${devices.tablet} {
    max-width: 45%;
  }
@media ${devices.laptop} {
    max-width: 30%;
  }
@media ${devices.desktop} {
    max-width: 23%;
  }
`
const Row = styled.div`
display: flex;
justify-content: space-between;
width: 100%;
height: 100%;
`
const Box = styled.div`
width: 25%;
display: flex;
justify-content: center;
align-items: center;
${props => props.border && `border-top: 1px solid #333;`}
${props => props.width && `width: ${props.width}`}
`
const Image = styled.img`
width: 130%;
height: auto;
`
const Info = styled.div`
font-size: .75rem;
margin: 0 20px;
width: 55%;
display: flex;
flex-direction: column;
justify-content: center;
`
const Name = styled.p`
text-align: left;
font-weight: bold;
font-size: 1rem;
`
const Description = styled.p`
  text-align: justify;
  line-height: 1.4;
  font-style: italic;
  max-height: 5rem;
  overflow-y: auto;
  color: #555;
`
const Add = styled.div`
width: 20%;
min-height: 100%;
display: flex;
flex-direction: column;
justify-content: space-between;
align-items: flex-end;
`
const Price = styled.p`
font-size: 1.2rem;
`
const Icon = styled.div`
width: 22px;
height: 22px;
border: 2px solid #B00;
border-radius: 50%;
font-size: 22px;
display: flex;
justify-content: center;
align-items: center;
margin-bottom: 5px;
cursor: pointer;
::after{
  content: "+";
  color: #B00;
  font-weight: 800;
}
`
const CardBurger = () => {
  
  const [data, setData] = useState([]);

  useEffect(()=>{
    let mounted = true;
    getList()
      .then(items =>{
        if(mounted){
          setData(items)
        }
      })
    return () => mounted=false;
  },[]);

  function getList() {
    return fetch('https://my-burger-api.herokuapp.com/burgers')
      .then(response => response.json());
  }

  return(
    <Container>
      {data.length>0 && data.map(item => 
        <Card>
          <Row>
            <Box>
              <Image src={'https://img.freepik.com/foto-gratis/deliciosa-hamburguesa-queso_1232-503.jpg'}/>
            </Box>
            <Info>
              <Name>{item.name}</Name>
              <Description>{item.description}</Description>
            </Info>
            <Add>
              <Price>${item.id}.90</Price>
              <Icon/>
            </Add>
          </Row>
        </Card>    
      )}
    </Container>
  )
}

export default CardBurger
