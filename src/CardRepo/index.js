import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import data  from './data'

const sizes = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px',
};

const devices = {
  mobileS: `(min-width: ${sizes.mobileS})`,
  mobileM: `(min-width: ${sizes.mobileM})`,
  mobileL: `(min-width: ${sizes.mobileL})`,
  tablet: `(min-width: ${sizes.tablet})`,
  laptop: `(min-width: ${sizes.laptop})`,
  laptopL: `(min-width: ${sizes.laptopL})`,
  desktop: `(min-width: ${sizes.desktop})`,
};

const Container = styled.div`
display: flex;
flex-wrap: wrap;
justify-content: center;
align-items: baseline;
align-content: center;
width: 100%;
overflow-y: auto;
background-color: #DDD;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Card = styled.div`
background-color: #FFF;
color: #000;
position: relative;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
align-content: center;
width: 100%;
height: fit-content;
padding: 15px 20px;
margin: 10px;
@media ${devices.tablet} {
    max-width: 45%;
  };
@media ${devices.laptop} {
    max-width: 30%;
  };
@media ${devices.desktop} {
    max-width: 23%;
  };
${props => props.state=='created' && `background-color: #AAF;`};
${props => props.state=='development' && `background-color: #FFA;`};
${props => props.state=='production' && `background-color: #AFA;`};
${props => props.state=='deprecated' && `background-color: #FAA;`};
`
const Row = styled.div`
display: flex;
justify-content: space-between;
width: 100%;
height: 100%;
`
const Box = styled.div`
width: 25%;
display: flex;
justify-content: center;
align-items: center;
${props => props.border && `border-top: 1px solid #333;`}
${props => props.width && `width: ${props.width}`}
`
const Image = styled.img`
width: 130%;
height: auto;
`
const Info = styled.div`
font-size: .75rem;
margin: 0 20px;
width: 55%;
display: flex;
flex-direction: column;
justify-content: center;
`
const Text= styled.p`
text-align: left;
font-size: 1rem;
margin: 5px;
${props => props.bold && `font-weight: bold;`}
`
const Icon = styled.div`
width: 22px;
height: 22px;
border: 2px solid #B00;
border-radius: 50%;
font-size: 22px;
display: flex;
justify-content: center;
align-items: center;
margin-bottom: 5px;
cursor: pointer;
::after{
  content: "+";
  color: #B00;
  font-weight: 800;
}
`
const CardRepo = () => {


  return(
    <Container>
      {data.length>0 && data.map(item => 
        <Card state={item.state}>
          <Text bold>{item.name}</Text>
          <Text>{item.randomid}</Text>
        </Card>    
      )}
    </Container>
  )
}

export default CardRepo
