import React, { useState } from 'react';
import styled from 'styled-components';

const sizes = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px',
};

const devices = {
  mobileS: `(min-width: ${sizes.mobileS})`,
  mobileM: `(min-width: ${sizes.mobileM})`,
  mobileL: `(min-width: ${sizes.mobileL})`,
  tablet: `(min-width: ${sizes.tablet})`,
  laptop: `(min-width: ${sizes.laptop})`,
  laptopL: `(min-width: ${sizes.laptopL})`,
  desktop: `(min-width: ${sizes.desktop})`,
};

const Card = styled.div`
background-color: #FFF;
color: #000;
position: relative;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
align-content: center;
width: 100%;
height: fit-content;
padding: 25px;
margin: 0px auto;
border-radius: 7px;
box-shadow: 2px 2px 5px 1px #777;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
@media ${devices.tablet} {
    max-width: 45%;
  }
@media ${devices.laptop} {
    max-width: 30%;
  }
@media ${devices.desktop} {
    max-width: 23%;
  }
`
const Row = styled.div`
position: relative;
display: flex;
justify-content: space-between;
width: 100%;
height: 100%;
${props => props.bordered && `
  justify-content:center;
  align-items:center;
  border: 1px solid #CCC;
  border-radius: 3px;
  padding: 5px;
  margin-top: 20px;
  `}
`
const Box = styled.div`
position: relative;
width: 100%;
display: flex;
flex-direction: column;
justify-content:center;
align-items:center;
align-content: center;
border: 1px solid #CCC;
border-radius: 3px;
margin-top: 20px;
overflow: visible;
${props => props.border && `border-top: 1px solid #333;`}
`
const Title = styled.p`
font-size: 1.2rem;
color: #444;
margin-bottom: 30px;
`
const Text = styled.p`
width: 100%;
text-align: left;
font-weight: bold;
font-size: .8rem;
color: #555;
`
const List = styled.ul`
position: absolute;
top: 0px;
left: -1px;
background-color: #FFF;
list-style: none;
text-align: left;
width: 100%;
height: fit-content;
border-radius: 3px;
overflow-y: auto;
z-index: 5;
transition: max-height 0.5s 0s ease-in-out;
${props => props.show==false && `max-height: 0px; border: none;`};
${props => props.show==true && `max-height: 150px; border: 1px solid #CCC;`};
`
const Item = styled.li`
font-size: .9rem;
color: #666;
padding: 3px 5px;
&:hover{
  color: #FFF;
  background-color: #4C88FE;
};
${props => props.selected && `color: #FFF; background-color: #3C78EE`}
`
const Selected = styled.p`
font-size: .9rem;
color: #666;
padding: 3px;
display: flex;
align-items: center;
justify-content: flex-start;
`
const Button = styled.button`
font-weight:800;
color: #fff;
background-color: #3C78EE;
width: 50%;
border: none;
padding: .5rem;
border-radius: 3px;
`
const Nota = styled.p`
font-size: .7rem;
color: #666;
margin-top: 25px;
&::before{
  content: '⚠';
  margin-right: 5px;
}
`
const Time = styled.p`
  width: 50%;
  text-align: center;
  font-weight: bold;
  font-size: .9rem;
  color: #555;
`
const Link = styled.p`
font-size: .7rem;
font-weight: bold;
color: #3C78EE;
margin-top: 20px;
`
const Close = styled.img`
  width: 15px;
  height: 15px;
  cursor: pointer;
  opacity: 0.75;
  margin: 0px;
`
const Icon = styled.p`
  cursor: pointer;
  font-size: 1rem;
  color: #666;
  padding: 5px;  
  transition: all 0.5s 0s ease-in-out;
  ${props => props.state==true&& `transform: rotate(90deg);`}
  ${props => props.state==false&& `transform: rotate(-90deg);`}
`
const CardEmpezarProy = () => {
  
  const [date, setDate] = useState(new Date());
  const [selected, setSelected] = useState(0);
  const [showList, setShowList] = useState(false);

  setTimeout(() => {
        setDate(new Date());
  }, 1000);

  const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

  const projects = [
    "Proyecto aaaaa",
    "Proyecto bbbbb",
    "Proyecto ccccc",
    "Proyecto ddddd",
    "Proyecto fffff",
    "Proyecto ggggg",
    "Proyecto hhhhh",
    "Proyecto iiiii",
    "Proyecto jjjjj",
  ];

  function getSelected(e,index) {
    e.preventDefault();
    setSelected(index);
    setShowList(false);
  }

  function changeShowList(e){
    e.preventDefault();
    setShowList(!showList);
  }

  return(
    <Card>
      <Row>
        <Title>Hoy: {('0'+date.getDate()).slice(-2)} {months[date.getMonth()]}, {date.getFullYear()}</Title>
        <Close src={'https://www.nicepng.com/png/detail/128-1281858_deletex-comments-delete-icon-svg.png'}/>
      </Row>
      <Text>Selecciona un proyecto</Text>
      <Box>
        <Row onClick={e=>changeShowList(e)}>
          <Selected>{projects[selected]}</Selected>
          <Icon state={showList}>&lt;</Icon>
        </Row>
        <Row>
          <List show={showList}>
            {projects.map((item, i) => 
              {
                {return (selected === i)? 
                  <Item selected onClick={e=>getSelected(e,i)}>{item}</Item>
                  :
                  <Item onClick={e=>getSelected(e,i)}>{item}</Item> 
                }
              }
            )}
          </List>
        </Row>
        
      </Box>
      
      <Row bordered>
        <Button>Empezar</Button>
        <Time>{('0'+date.getHours()).slice(-2)}:{('0'+date.getMinutes()).slice(-2)}:{('0'+date.getSeconds()).slice(-2)}</Time>
      </Row>
      <Nota>Se sobreescribe si coincide con entradas manuales</Nota>
      <Link>Ver mis registros</Link>
    </Card>    
  )
}

export default CardEmpezarProy
