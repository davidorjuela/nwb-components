import React from 'react';
import styled from 'styled-components';

const Card = styled.div`
font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
background-color: #000;
color: #FFF;
position: relative;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
width: 400px;
height: fit-content;
overflow: hidden;
margin: 0px auto;
padding: 50px;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Row = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
width: 100%;
margin: 10px 0px;
position: relative;
${props => props.jCCenter && 'justify-content: center;'}
${props => props.margin && `margin: ${props.margin}`}
`
const Icon = styled.div`
font-size: 1.7rem;
font-weight: 300;
background-color: #8D44AD;
padding: 1.2rem 1rem;
`
const Arrows = styled.div`
font-size: 3rem;
margin: 0px 10px;
`
const Profile = styled.div`
background-color: #000;
width: 100%;
border: 1px solid #333;
border-radius: 3px;
margin: 10px 0px 5px 0px;
padding: 12px 5px;
z-index: 5;
`
const Rombo = styled.div`
width: 1rem;
height: 1rem;
background-color: #333;
transform: rotate(45deg);
position: absolute;
top: .4rem;
right: calc(150px - .5rem);
z-index: 4;
`
const Title = styled.p`
font-weight: 500;
font-size: 1.4rem;
margin-top: 25px;
`
const Text = styled.p`
font-size: .9rem;
margin: 7px 0px;
text-align: center;
`
const Span = styled.span`
font-weight: 700;
&:before {
    content: "○ ";
  }
`
const Button = styled.button`
  background-color: #000;
  color: white;
  font-size: .9rem;
  font-weight: 100;
  padding: 15px 45px;
  border-radius: 3px;
  border: 1px solid #333;
  ${props => props.accept && `background-color: #00B3FE`};
  ${props => props.decline && `background-color: #000`}
`
const CardDocker = ({email, iniciales}) => {
  return(
    <Card>
      <Text>Docker</Text>
      <Title>Authorize App</Title>
      <Row jCCenter={true} margin={'20px 0px'}>
        <Icon>{iniciales}</Icon>
        <Arrows>&#8644;</Arrows>
        <Text>Docker Forums</Text>
      </Row>
      <Text>Hi {email},</Text>
      <Text>Docker Forums is requesting access to your Docker account.</Text>
      <Row>
        <Profile>
          <Text><Span>Profile: </Span>access to your profile and email</Text>
        </Profile>
        <Rombo />
      </Row>
      <Row>
        <Button decline>Decline</Button>
        <Button accept>Accept</Button>
      </Row>
    </Card>
  )
}

export default CardDocker
