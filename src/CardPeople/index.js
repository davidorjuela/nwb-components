import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

const sizes = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px',
};

const devices = {
  mobileS: `(min-width: ${sizes.mobileS})`,
  mobileM: `(min-width: ${sizes.mobileM})`,
  mobileL: `(min-width: ${sizes.mobileL})`,
  tablet: `(min-width: ${sizes.tablet})`,
  laptop: `(min-width: ${sizes.laptop})`,
  laptopL: `(min-width: ${sizes.laptopL})`,
  desktop: `(min-width: ${sizes.desktop})`,
};

const Container = styled.div`
display: flex;
flex-wrap: wrap;
justify-content: center;
align-items: baseline;
align-content: center;
background-color: #FFF;
width: 100%;
max-width: 800px;
margin: 0px auto;
padding: 25px;
border-radius: 10px;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Card = styled.div`
background-color: #FFF;
position: relative;
display: flex;
flex-direction: column;
justify-content: space-around;
align-items: center;
width: 50%;
height: fit-content;
padding: 15px 20px;
@media ${devices.tablet} {
    max-width: 33%;
  }
@media ${devices.laptop} {
    max-width: 25%;
  }
@media ${devices.desktop} {
    max-width: 20%;
  }
`
const Close = styled.img`
  width: 15px;
  height: 15px;
  position:absolute;
  right: -5px;
  top:-5px;
  cursor: pointer;
  opacity: 0.75;
  margin: 0px;
`
const Box = styled.div`
position: relative;
display: flex;
flex-wrap: wrap;
justify-content: center;
align-items: center;
${props => props.column && `flex-direction: column;`};
${props => props.title && `margin: 10px auto 50px auto;`};
`
const Icon = styled.div`
width: 1rem;
height: 1rem;
border-radius: 50%;
background-color: green;
position: absolute;
top: .5rem;
right: .5rem;
`
const Image = styled.img`
width: 7rem;
height: auto;
border-radius: 50%;
`
const Text = styled.p`
font-size: .8rem;
color: #333;
${props => props.bolder && `font-weight: 800; font-size: .9rem`};
${props => props.title && `font-weight: 800; font-size: 1.5rem; margin-bottom: 10px;`};
`
const CardPeople = () => {

  const [data, setData] = useState([]);

  useEffect(()=>{
    let mounted = true;
    getList()
      .then(data =>{
        if(mounted){
          data.data[2].online= true;
          data.data[4].online= true;
          setData(data.data)
        }
      })
    return () => mounted=false;
  },[]);

  function getList() {
    return fetch('https://reqres.in/api/users?page=1')
      .then(response => response.json());
  }

  return(
    <Container>
      <Box column content>
        <Close src={'https://www.nicepng.com/png/detail/128-1281858_deletex-comments-delete-icon-svg.png'} />
        <Box column title>
          <Text title>Brie's space</Text>
          <Text>What game needs a reboot but hasn't gotten one?</Text>
        </Box>
        <Box>
          {data.map(user => 
            <Card>
              <Box>
                <Image src={user.avatar}/>
                {user.online &&
                  <Icon />
                }
              </Box>
              <Text bolder>{user.first_name}</Text>
              <Text>{user.email}</Text>
            </Card>    
          )}
        </Box>
      </Box>
    </Container>
  )
}

export default CardPeople
