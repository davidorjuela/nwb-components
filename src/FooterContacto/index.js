import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
html{
  box-sizing: border-box;
  margin: 0px;
  padding: 0px;
  font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
}
*, *:before, *:after {
    box-sizing: inherit;
    margin: 0px;
    padding: 0px;
}
`
const Card = styled.div`
background-color: #000;
color: #FFF;
position: relative;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
width: 100%;
max-width: 800px;
height: fit-content;
overflow: hidden;
margin: 0px auto;
padding: 50px;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Row = styled.div`
display: flex;
justify-content: space-between;
align-items: flex-start;
width: 100%;
margin: 10px 0px;
position: relative;
${props => props.jCStart && 'justify-content: flex-start;'}
${props => props.margin && `margin: ${props.margin}`}
`
const Box = styled.div`
width: 47%;
${props => props.border && `border-top: 1px solid #333;`}
${props => props.width && `width: ${props.width}`}
`
const Title = styled.p`
font-weight: 800;
font-size: .9rem;
color: #71FFFF;
`
const Description = styled.p`
font-size: 1.5rem;
font-weight: lighter;
margin: 20px 0px 35px 0px;
`
const H2 = styled.h2`
font-size: .8rem;
font-weight: 300;
`
const Text = styled.p`
font-size: .75rem;
margin: 10px 0px;
text-align: left;
`
const Link = styled.span`
font-weight: 700;
color: #71FFFF;
text-decoration: underline;
`
const Image = styled.img`
border-radius: 50%;
width: 4rem;
height: auto;
margin-right: 10px;
`

const Info = styled.div`
font-size: .75rem;
text-align: left;
`

const Name = styled.p`
font-weight: bold;
font-size: .8rem;
`

const Job = styled.p`
font-stretch: condensed;
margin-top: 5px;
`
const Email = styled.p`
color: #71FFFF;
margin-top: 10px;
`

const FooterContacto = ({ }) => {
  return(
    <Card>
      <GlobalStyle />
      <Title>CONTACTO</Title>
      <Description>¿Alguna duda? Estaremos encantados de atenderle.</Description>
      <Row>
        <Box>
          <H2>Formas de contacto</H2>
          <Box width={'100%'} border>
            <Text>
            Use nuestro <Link>formulario de contacto</Link> o consulte la seccion de <Link>preguntas frecuentes</Link>.
            </Text>
            <Text>Tambien puede ponerse en contacto directamente con nuestro servicio de atención al cliente.</Text>
          </Box>
        </Box>
        <Box>
          <H2>Su contacto con la sala de prensa e infografías</H2>
          <Box width={'100%'} border>
            <Row jCStart>
              <Image src={'https://cdn.statcdn.com/contactPerson/2580.jpg'}/>
              <Info>
                <Name>Mónica Mena Roa</Name>
                <Job>Data journalist</Job>
                <Email>monica.menaroa@statista.com</Email>
              </Info>
            </Row>
          </Box>
        </Box>
      </Row>
    </Card>
  )
}

export default FooterContacto
