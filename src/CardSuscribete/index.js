import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
html{
  box-sizing: border-box;
  margin: 0px;
  padding: 0px;
}
*, *:before, *:after {
    box-sizing: inherit;
    margin: 0px;
    padding: 0px;
}
`
const Card = styled.div`
font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
background-color: #FFF;
position: relative;
display: flex;
flex-direction: column;
justify-content: center;
align-content: center;
width: 600px;
height: fit-content;
color: #222;
overflow: hidden;
margin: 0pc auto;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Content = styled.div`
position: relative;
display: flex;
justify-content: center;
align-items: center;
width: 100%;
padding: 25px;
`
const Best = styled.div`
background-color: #FFF;
position: relative;
display: flex;
justify-content: flex-start;
align-items: flex-end;
height: 50px;
width: 100%;
`
const Liston = styled.div`
background-color: #00AF80;
text-align: left;
color: #FFF;
padding: 5px 25px;
font-size: .8rem;
width: 30%;
z-index: 6;
`
const Rombo = styled.div`
width: calc(0.8rem + 11px);
height: calc(0.8rem + 11px);
background-color: #FFF;
transform: rotate(45deg);
position: absolute;
left: calc(30% - (11px + .8rem)/2);
z-index: 7;
`
const Lside = styled.div`
width: 60%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-start;
`
const Rside = styled.div`
width: 40%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-end;
`
const Meses = styled.p`
font-weight: 1000;
font-size: .9rem;
`
const PrecioMes = styled.p`
  font-size: .8rem;
`
const Span = styled.span`
font-weight: 800;
`
const Span1 = styled.span`
  color: #00AF80;
  font-weight: 800;
  font-size: 1.5rem;
  text-decoration: line-through;
  margin-right: 5px;
`

const Span2 = styled.span`
  color: #00AF80;
  font-weight: 800;
  font-size: 1.5rem;
  margin-right: 5px;
`
const PrecioTotal = styled.p`
font-size: .8rem;
`
const Nota = styled.p`
  margin: 15px 0px 0px 0px;
  font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
  font-weight: 600;
  font-size: 0.7rem;
`
const Button = styled.button`
  background-color: #4BD395;
  color: white;
  font-size: .9rem;
  font-weight: 100;
  padding: 15px 30px;
  border-radius: 3px;
  border: none;
`

const CardSuscribete = ({precioMes, precioMesPromo, periocidad, mejorOpcion, moneda}) => {
  return(
    <Card>
      <GlobalStyle />
      {mejorOpcion &&
        <Best>
          <Liston>
            Mejor opción
          </Liston>
          <Rombo />
        </Best>
      }
      <Content>
        <Lside>
          <Meses>{periocidad}
            { periocidad > 1 ?  <> meses</> : <> mes</> }
          </Meses>
          <PrecioMes><Span1>{precioMes.toFixed(2)} {moneda}</Span1><Span2>{precioMesPromo.toFixed(2)} {moneda}</Span2>/mes</PrecioMes>
          <PrecioTotal>
            <Span>{(precioMesPromo*periocidad).toFixed(2)} {moneda}</Span> cada 
              { periocidad > 1 ?  <> <Span> {periocidad} </Span> meses</> : <> mes</> }
          </PrecioTotal>
          <Nota>* Se pueden aplicar otro tipo de impuestos locales</Nota>
        </Lside>
        <Rside>
          <Button>Suscribete ya</Button>
        </Rside>  
      </Content>
    </Card>
  )
}

export default CardSuscribete
