import React from 'react';
import styled from 'styled-components';
import data  from './data'

const sizes = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px',
};

const devices = {
  mobileS: `(min-width: ${sizes.mobileS})`,
  mobileM: `(min-width: ${sizes.mobileM})`,
  mobileL: `(min-width: ${sizes.mobileL})`,
  tablet: `(min-width: ${sizes.tablet})`,
  laptop: `(min-width: ${sizes.laptop})`,
  laptopL: `(min-width: ${sizes.laptopL})`,
  desktop: `(min-width: ${sizes.desktop})`,
};

const Container = styled.div`
display: flex;
flex-wrap: wrap;
justify-content: center;
background-color: #CCC;
padding: 10px;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
`
const Card = styled.div`
background-color: #FFF;
color: #000;
display: inline-flex;
flex-direction: column;
justify-content: space-between;
align-items: stretch;
width: 100%;
height: 400px;
padding: 15px 20px;
margin: 10px;
@media ${devices.tablet} {
    max-width: 43%;
  }
@media ${devices.laptop} {
    max-width: 31%;
  }
@media ${devices.desktop} {
    max-width: 23%;
  }
`
const Row = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
width: 100%;
margin-top: 15px;
${props => props.lessmargin && `margin-top: 5px;`}
`
const Box = styled.div`
width: 100%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-content: center;
${props => props.yscroll && `
  display: block;
  overflow-y: auto;
  margin-top: 10px;
  `}
`
const Image = styled.img`
width: 80px;//50px;
height: 50px;
//border-radius: 50%;
`
const Text = styled.p`
font-size: .9rem;
color: #555;
margin: 0px 5px;
text-align: left;
${props => props.bolder && `font-weight: 800; color: #333`}
`
const Button = styled.div`
  opacity: 0.5;
  padding: 5px 20px;
  margin-left: 15px;
  border-radius: 3px;
  ${props => props.pointer&& `cursor: pointer;`};
  ${props => props.margin&& `
    &::before {
      margin-right: 5px;
    }`};
  ${props => props.state=="Completed" && `
    color: green; 
    border: 1px solid green;
    &::before {
      content: '✔';
    }`};
  ${props => props.state=="Rejected" && `
    color: red; 
    border: 1px solid red;
    &::before {
      content: '✗';
    }`};
`
const CardOrder = () => {
  return(
    <Container>
      {data.orders.map(order => 
        <Card>
          <Row>
            <Box>
              <Text bolder>Order #{order.id}</Text>
              <Text>{order.date}</Text>
            </Box>
            <Image src={order.user.image}/>
          </Row>
          <Box yscroll>
            {order.items.map(item => 
              <Row>
                <Image src={item.image}/>
                <Box>
                  <Text bolder>{item.name}</Text>
                  <Text>{item.description}</Text>
                  <Row lessmargin>
                    <Text bolder>${item.price}</Text><Text bolder>Qty: {item.qty}</Text>
                  </Row>
                </Box>
              </Row>
            )}
          </Box>
          <Row>
            <Box>
              <Text>X{order.qty} items</Text>
              <Text bolder>${order.price}</Text>
            </Box>
              {(order.state=="Created") ?
                <>
                  <Button pointer state={'Completed'} />
                  <Button pointer state={'Rejected'} />
                </>
                :
                <Button margin state={order.state}>{order.state}</Button>
              }
          </Row>
        </Card>    
      )}
    </Container>
  )
}

export default CardOrder
