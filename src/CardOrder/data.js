const data =

{
    orders:[
        {
            "id": 123,
            "date": "03 Abril 2022 22:19:35",
            "state": "Created",
            "qty": 5,
            "price": 40.99,
            "user": {
                "id": "1wadadwadawsef",
                "image": "https://e7.pngegg.com/pngimages/78/788/png-clipart-computer-icons-avatar-business-computer-software-user-avatar-child-face.png"
            },
            "items": [
                {
                    "id": "aresererw3rw0",
                    "name": "Krab burger0",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                },
                {
                    "id": "aresererw3rw1",
                    "name": "Krab burger1",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                },
                {
                    "id": "aresererw3rw2",
                    "name": "Krab burger2",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                },
                {
                    "id": "aresererw3rw3",
                    "name": "Krab burger3",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                },
                {
                    "id": "aresererw3rw4",
                    "name": "Krab burger4",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                }
            ]
        },
        {
            "id": 124,
            "date": "03 Abril 2022 22:19:35",
            "state": "Rejected",
            "qty": 2,
            "price": 40.99,
            "user": {
                "id": "1wadadwadawsef",
                "image": "https://e7.pngegg.com/pngimages/78/788/png-clipart-computer-icons-avatar-business-computer-software-user-avatar-child-face.png"
            },
            "items": [
                {
                    "id": "aresererw3rw0",
                    "name": "Krab burger0",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                },
                {
                    "id": "aresererw3rw1",
                    "name": "Krab burger1",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                }
            ]
        },
        {
            "id": 125,
            "date": "03 Abril 2022 22:19:35",
            "state": "Created",
            "qty": 4,
            "price": 40.99,
            "user": {
                "id": "1wadadwadawsef",
                "image": "https://e7.pngegg.com/pngimages/78/788/png-clipart-computer-icons-avatar-business-computer-software-user-avatar-child-face.png"
            },
            "items": [
                {
                    "id": "aresererw3rw0",
                    "name": "Krab burger0",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                },
                {
                    "id": "aresererw3rw1",
                    "name": "Krab burger1",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                },
                {
                    "id": "aresererw3rw2",
                    "name": "Krab burger2",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                },
                {
                    "id": "aresererw3rw3",
                    "name": "Krab burger3",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                }
            ]
        },
        {
            "id": 126,
            "date": "03 Abril 2022 22:19:35",
            "state": "Completed",
            "qty": 1,
            "price": 40.99,
            "user": {
                "id": "1wadadwadawsef",
                "image": "https://e7.pngegg.com/pngimages/78/788/png-clipart-computer-icons-avatar-business-computer-software-user-avatar-child-face.png"
            },
            "items": [
                {
                    "id": "aresererw3rw0",
                    "name": "Krab burger0",
                    "description": "Ñam ñam ñam esto es una prueba de dscripcion",
                    "image": "https://e7.pngegg.com/pngimages/143/317/png-clipart-cheeseburger-hamburger-whopper-fast-food-mcdonald-s-big-mac-bread-cheeseburger-hamburger.png",
                    "price": 10.99,
                    "qty": 1
                }
            ]
        }
    ]
}

export default data