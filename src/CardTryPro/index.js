import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

const sizes = {
  mobileS: '320px',
  mobileM: '375px',
  mobileL: '425px',
  tablet: '768px',
  laptop: '1024px',
  laptopL: '1440px',
  desktop: '2560px',
};

const devices = {
  mobileS: `(min-width: ${sizes.mobileS})`,
  mobileM: `(min-width: ${sizes.mobileM})`,
  mobileL: `(min-width: ${sizes.mobileL})`,
  tablet: `(min-width: ${sizes.tablet})`,
  laptop: `(min-width: ${sizes.laptop})`,
  laptopL: `(min-width: ${sizes.laptopL})`,
  desktop: `(min-width: ${sizes.desktop})`,
};

const Card = styled.div`
background-color: #000;
color: #FFF;
position: relative;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
align-content: center;
width: 100%;
height: fit-content;
padding: 25px 30px 40px 30px;
margin: 0px auto;
animation: fadein 2s;
@keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
}
@media ${devices.tablet} {
    max-width: 45%;
  }
@media ${devices.laptop} {
    max-width: 30%;
  }
@media ${devices.desktop} {
    max-width: 23%;
  }
`
const Row = styled.div`
display: flex;
justify-content: space-between;
width: 100%;
height: 100%;
margin: 10px 0px;
${props => props.noMargin && `margin-top:-5px`};
${props => props.bordered && `
  border-top: 1px solid #555;
  border-bottom: 1px solid #555;
  padding: 25px 10px;
`}
`
const Box = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
width: 100%;
${props => props.border && `border-top: 1px solid #333;`}
${props => props.width && `width: ${props.width}`}
`
const WTF = styled.div`
width: 3rem;
height: 2rem;
font-size: .2rem;
background-color: #4800F0;
border-radius: .2rem;
padding: .2rem;
margin: 15px 15px 0px 0px;
`
const Title = styled.p`
  width: 100%;
  text-align: left;
  font-weight: bold;
  font-size: 2.3rem;
  margin: 7px 0px;
`
const Text = styled.p`
  width: 100%;
  font-size: .9rem;
  color: #FFF;
  ${props => props.gray && `color: #555;`};
  ${props => props.bold && `font-weight: bold;`};
  ${props => props.bolder && `font-weight: bolder;`};
  ${props => props.align && `text-align: ${props.align};`};
`
const Description = styled.p`
  width: 100%;
  text-align: left;
  font-size: 1rem;
  font-weight: bold;
  color: #AAA;
`
const Span = styled.p`
  width: 100%;
  color: #555;
  text-align: right;
  font-size: .8rem;
`

const CardTryPro = () => {
  
  const [data, setData] = useState([]);

  useEffect(()=>{
    let mounted = true;
    getList()
      .then(items =>{
        if(mounted){
          setData(items)
        }
      })
    return () => mounted=false;
  },[]);

  function getList() {
    return fetch('https://my-burger-api.herokuapp.com/burgers')
      .then(response => response.json());
  }

  return(
    <Card>
      <Box>
        <Description>Try Pro ($15/month)</Description>
        <Title>30 days free</Title>
        <Text bold gray align={'left'}>Then $15.00 per month</Text>
      </Box>
      <Row>
        <WTF>
          <h2>WTF</h2>
          <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, 
            consectetur, adipisci velit..."</p>
        </WTF>
        <Box>
          <Row>
            <Text bold align={'left'}>Pro($15/month)</Text>
            <Text bold align={'right'}>30 days free</Text>
          </Row>
          <Row noMargin>
            <Span>$15.00/month after</Span>
          </Row>
          <Row>
            <Text bolder align={'left'}>Subtotal</Text>
            <Text bolder align={'right'}>$15.00</Text>
          </Row>
          <Row bordered>
            <Text bold gray align={'left'}>Add promotion code</Text>
          </Row>
          <Row>
            <Text align={'left'}>Total after trial</Text>
            <Text align={'right'}>$15.00</Text>
          </Row>
          <Row>
            <Text bolder align={'left'}>Total due today</Text>
            <Text bolder align={'right'}>$0.00</Text>
          </Row>
        </Box>
      </Row>
    </Card>    
  )
}

export default CardTryPro
